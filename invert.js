function invert(obj) {
    if(!obj){
        return []
    }
    let newObject={}
    for (let key in obj){
        newObject[obj[key]]=key;
    }
    return newObject;
}

module.exports=invert;