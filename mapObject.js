function mapObject(obj, cb) {
    if(!obj){
        return [];
    }
    for(let item in obj){
        let output=cb(obj[item])
        obj[item]=output;
    }
    return obj
};

module.exports=mapObject;