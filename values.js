function values(obj) {
    if(!obj){
        return []
    }
    let newArray=[]
    for (let key in obj){
        newArray.push(obj[key]);
    }
    return newArray;
}

module.exports=values;