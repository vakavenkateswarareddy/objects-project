function keys(obj) {
    if(!obj){
        return []
    }
    let newArray=[]
    for (let key in obj){
        newArray.push(key);
    }
    return newArray;
}

module.exports=keys;
