function pairs(obj) {
    if(!obj){
        return []
    }
    let newArray=[]
    for (let key in obj){
        newArray.push([key,obj[key]]);
    }
    return newArray;
}

module.exports=pairs;
